<?php
/**
 * Wolf CMS authentication backend for Dokuwiki
 *
 * Uses external Trust mechanism to check against Wolf's user cookie.
 *
 * @author    Martijn van der Kleijn <martijn.niji@gmail.com>
 * 
 * 
 * YOU WILL NEED TO ADD THESE LINES TO YOUR DOKUWIKI local.php
 
$conf['authtype'] = 'wolfcms';
$conf['passcrypt'] = 'md5';
$conf['superuser'] = '@administrator';
$conf['manager'] = '@editor';
$conf['disableactions'] = 'register,resendpwd,login';

 * 
 */

//---- DO WOLF CMS INIT STUFF ----
define('CMS_ROOT', '/home/wolfcms/www');
require CMS_ROOT.'/config.php';
require CMS_ROOT.'/wolf/Framework.php';
require CMS_ROOT.'/wolf/app/models/User.php';
require CMS_ROOT.'/wolf/app/models/AuthUser.php';

try {
    $__CMS_CONN__ = new PDO(DB_DSN, DB_USER, DB_PASS);
} 
catch (PDOException $error) {
    die('DB Connection failed: '.$error->getMessage());
}
    
if ($__CMS_CONN__->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
    $__CMS_CONN__->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
}

Record::connection($__CMS_CONN__);
Record::getConnection()->exec("set names 'utf8'");

function __autoload($class_name) {
    return;
}


//---- BEGIN DOKUWIKI STUFF ----
require_once(DOKU_INC.'inc/auth/mysql.class.php');

class auth_wolfcms extends auth_mysql {
	function auth_wolfcms() {
		$this->cando['external'] = true;
                $this->cando['logoff'] = true;
 
		global $conf;
                $conf['passcrypt'] = 'md5';

                $conf['auth']['mysql']['forwardClearPass'] = 0;
                $conf['auth']['mysql']['TablesToLock']= array("wolf_user", "wolf_user AS u", "wolf_permission", "wolf_permission AS p", "wolf_user_permission", "wolf_user_permission AS up");
                $conf['auth']['mysql']['checkPass']   = "SELECT password AS pass
                                                         FROM wolf_user
                                                         WHERE username='%{user}'";
                $conf['auth']['mysql']['getUserInfo'] = "SELECT password AS pass, name, email AS mail
                                                         FROM wolf_user
                                                         WHERE username='%{user}'";
                $conf['auth']['mysql']['getGroups']   = "SELECT p.name as `group`
                                                         FROM wolf_permission p, wolf_user u, wolf_user_permission up
                                                         WHERE u.id = up.user_id
                                                         AND p.id = up.permission_id
                                                         AND u.username='%{user}'";
                $conf['auth']['mysql']['getUsers']    = "SELECT DISTINCT username AS user
                                                         FROM wolf_user AS u
                                                         LEFT JOIN wolf_user_permission AS up ON u.id=up.user_id
                                                         LEFT JOIN wolf_permission AS p ON up.permission_id=p.id";
                $conf['auth']['mysql']['FilterLogin'] = "username LIKE '%{user}'";
                $conf['auth']['mysql']['FilterName']  = "u.name LIKE '%{name}'";
                $conf['auth']['mysql']['FilterEmail'] = "email LIKE '%{email}'";
                $conf['auth']['mysql']['FilterGroup'] = "p.name LIKE '%{group}'";
                $conf['auth']['mysql']['SortOrder']   = "ORDER BY u.username";

 
                // CUSTOMIZE THESE SETTINGS!
		// now set up the mysql config strings
                $conf['auth']['mysql']['server'] = 'localhost';
                $conf['auth']['mysql']['user'] = 'your-wolf-cms-db-user';
                $conf['auth']['mysql']['password'] = 'your-wolf-cms-db-pwd';
                $conf['auth']['mysql']['database'] = 'your-wolf-cms-db-name';

		// call mysql constructor
		$this->auth_mysql();
	}
 
 
	function trustExternal($username, $password, $sticky = false) {
		global $USERINFO;
		global $conf;

		$sticky ? $sticky = true : $sticky = false; // sanity check
 
                // Start checking here
                AuthUser::load();

		// someone used the login form
		if(!empty($username) && !AuthUser::isLoggedIn()) {
			// run wolf's login function
			$login = AuthUser::login($username, $password, $sticky);

			if(!$login) {
                            $this->success = false;
                            return false;
                        }
		}

                if (!AuthUser::isLoggedIn()) { return false; }
 
                // Checks succeeded, set globals
                $user = $this->getUserData(AuthUser::getUserName());

		$USERINFO['name'] = $user['name'];
		$USERINFO['mail'] = $user['mail'];
                $USERINFO['grps'] = $user['grps'];
 
		$_SERVER['REMOTE_USER'] = AuthUser::getUserName();
		$_SESSION[DOKU_COOKIE]['auth']['user'] = AuthUser::getUserName();
		$_SESSION[DOKU_COOKIE]['auth']['pass'] = $user['pass'];
		$_SESSION[DOKU_COOKIE]['auth']['info'] = $USERINFO;

                $this->success = true;
 
		return true;
	}
 
 
	function logoff() {
                if (AuthUser::isLoggedIn()) { 
                    AuthUser::logout();
                }
	}
}

?>
